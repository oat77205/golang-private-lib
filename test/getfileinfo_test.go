package test

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system/services/getfileinfo"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
)

func Test_Getdocument_OrderId(t *testing.T) {
	logModel := logger.LogModel{Txid: uuid.New().String()}

	call := getfileinfo.NewCallGetFileInfo(GetHttpConfig())
	svc := getfileinfo.NewServiceGetFileInfo(call)
	req := getfileinfo.GetFileInfoRequest{
		RefID:           "",
		OrderId:         "800017011147",
		ApplicationCode: "tos",
	}
	res, e := svc.GetFileInfoService(req, logModel)
	if e != nil {
		// t.Error(e)
		t.Log(e)
	}
	t.Log(fmt.Sprintf("%+v", res))
}
