package getfile

import (
	"encoding/json"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceGetFile interface {
	GetFileService(GetFileRequest, logger.LogModel) (*GetFileResponse, error)
	GetFileByteService(GetFileRequest, logger.LogModel) ([]byte, error)
}

type serviceGetFile struct {
	GetFile ICallGetFile
}

func NewServiceGetFile(GetFile ICallGetFile) IServiceGetFile {
	return &serviceGetFile{
		GetFile: GetFile,
	}
}

func (r *serviceGetFile) GetFileService(req GetFileRequest, logModel logger.LogModel) (*GetFileResponse, error) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, err := r.GetFile.GetFile(req, logModel)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = resp.Code
			statusDescription = resp.Message
			if statusCode == "200" {
				statusCode = "200"
				statusDescription = "Success"
			} else {
				statusCode = "900"
				statusDescription = "GetFile fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}

	reqByte, _ := json.Marshal(req)
	// resByte, _ := json.Marshal(resp)

	logStepRequest := logger.LogStepRequest{
		StepName:     "HL-OTP - GetFile",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(reqByte),
		StepResponse: "Success", // string(resByte),
		Endpoint:     url,
		Method:       "GET",
		System:       "",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return resp, err
}

func (r *serviceGetFile) GetFileByteService(req GetFileRequest, logModel logger.LogModel) ([]byte, error) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, err := r.GetFile.GetFileByte(req, logModel)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	}

	reqByte, _ := json.Marshal(req)
	// resByte, _ := json.Marshal(resp)

	logStepRequest := logger.LogStepRequest{
		StepName:     "HL-OTP - GetFileByte",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(reqByte),
		StepResponse: "Success", // string(resByte),
		Endpoint:     url,
		Method:       "GET",
		System:       "",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return resp, err
}
