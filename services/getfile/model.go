package getfile

import "gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"

type GetFileRequest struct {
	RefID           string `json:"refId"`
	ApplicationCode string `json:"applicationCode"`
	MessageEncrypt  string `json:"messageEncrypt"`
	DUB             string `json:"dob"`
}

type GetFileResponse struct {
	Code     string         `json:"code"`
	BizError string         `json:"bizError"`
	Message  string         `json:"message"`
	File     string         `json:"file"`
	Errors   []utils.Errors `json:"errors"`
	System   string         `json:"system"`
}
