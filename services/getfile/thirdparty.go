package getfile

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type ICallGetFile interface {
	GetFile(GetFileRequest, logger.LogModel) (*GetFileResponse, string, error)
	GetFileByte(GetFileRequest, logger.LogModel) ([]byte, string, error)
}

type CallGetFile struct {
	config utils.EtcdApiConfig
}

func NewCallGetFile(conf utils.EtcdApiConfig) ICallGetFile {
	return &CallGetFile{
		config: conf,
	}
}

func (r *CallGetFile) GetFile(req GetFileRequest, logModel logger.LogModel) (*GetFileResponse, string, error) {

	startStep := time.Now()
	reqByte, _ := json.Marshal(req)

	logStepRequest := logger.LogStepRequest{
		StepName:    "Doc System - GetFile",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    r.config.Url,
		Method:      "GET",
		System:      "Doc System",
	}

	headers := map[string]string{}
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	headers["Content-Type"] = "application/json"
	headers["apiKey"] = r.config.Authentication.ApiKey

	var pathUrl string
	var found bool

	if pathUrl, found = r.config.Endpoints["getFile"]; !found {
		err := errors.New("endpoint getFile not found")
		return nil, r.config.Url, err
	}
	logStepRequest.Endpoint += pathUrl
	url := r.config.Url + pathUrl
	url = url + fmt.Sprintf("?refId=%s&applicationCode=%s", req.RefID, req.ApplicationCode)
	d, code, err := utils.Get(url, &headers, nil, &options)
	if err != nil {
		return nil, r.config.Url, err
	}

	if code != 200 {
		err = errors.New("StatusCode err from 3rd party " + strconv.Itoa(code))
		logStepRequest.Response = string(d)
		logStepRequest.ResultCode = fmt.Sprint(code)
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, r.config.Url, err
	}

	var response GetFileResponse
	if err := json.Unmarshal(d, &response); err != nil {
		logStepRequest.Response = string(d)
		logStepRequest.ResultCode = "500"
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, r.config.Url, err
	}
	resByte, _ := json.Marshal(response)
	logStepRequest.StepResponse = string(resByte)
	logStepRequest.ResultCode = "200"
	logStepRequest.ResultDesc = "Success"
	logger.LogStepPool(logStepRequest, logModel, startStep)
	return &response, r.config.Url, nil
}

func (r *CallGetFile) GetFileByte(req GetFileRequest, logModel logger.LogModel) ([]byte, string, error) {

	startStep := time.Now()
	reqByte, _ := json.Marshal(req)

	logStepRequest := logger.LogStepRequest{
		StepName:    "Doc System - GetFile",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    r.config.Url,
		Method:      "GET",
		System:      "Doc System",
	}

	headers := map[string]string{}
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	headers["Content-Type"] = "application/json"
	headers["apiKey"] = r.config.Authentication.ApiKey

	var pathUrl string
	var found bool

	if pathUrl, found = r.config.Endpoints["getFilebyte"]; !found {
		err := errors.New("endpoint getFile not found")
		return nil, r.config.Url, err
	}
	logStepRequest.Endpoint += pathUrl
	url := r.config.Url + pathUrl
	url = url + fmt.Sprintf("?refId=%s&applicationCode=%s", req.RefID, req.ApplicationCode)
	d, code, err := utils.Get(url, &headers, nil, &options)
	if err != nil {
		return nil, r.config.Url, err
	}

	if code != 200 {
		err = errors.New("StatusCode err from 3rd party " + strconv.Itoa(code))
		logStepRequest.Response = string(d)
		logStepRequest.ResultCode = fmt.Sprint(code)
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, r.config.Url, err
	}

	// var response GetFileResponse
	// if err := json.Unmarshal(d, &response); err != nil {
	// 	logStepRequest.Response = string(d)
	// 	logStepRequest.ResultCode = "500"
	// 	logStepRequest.ResultDesc = err.Error()
	// 	logger.LogStepPool(logStepRequest, logModel, startStep)
	// 	return nil, r.config.Url, err
	// }
	// resByte, _ := json.Marshal(response)
	// logStepRequest.StepResponse = string(resByte)
	// logStepRequest.ResultCode = "200"
	// logStepRequest.ResultDesc = "Success"
	// logger.LogStepPool(logStepRequest, logModel, startStep)
	return d, r.config.Url, nil
}
