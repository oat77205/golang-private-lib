package getfileinfo

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type ICallGetFileInfo interface {
	GetDocument(GetFileInfoRequest, logger.LogModel) (*GetFileInfoResponse, string, error)
	GetDocumentByOrderId(GetFileInfoRequest, logger.LogModel) (*GetFileInfoResponse, string, error)
}

type CallGetFileInfo struct {
	config utils.EtcdApiConfig
}

func NewCallGetFileInfo(conf utils.EtcdApiConfig) ICallGetFileInfo {
	return &CallGetFileInfo{
		config: conf,
	}
}

func (r *CallGetFileInfo) GetDocument(req GetFileInfoRequest, logModel logger.LogModel) (*GetFileInfoResponse, string, error) {

	startStep := time.Now()
	reqByte, _ := json.Marshal(req)

	logStepRequest := logger.LogStepRequest{
		StepName:    "Doc System - GetDocument",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    r.config.Url,
		Method:      "GET",
		System:      "Doc System",
	}

	headers := map[string]string{}
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	headers["Content-Type"] = "application/json"

	if r.config.Authentication.Basic.Password != "" {
		headers["apiKey"] = r.config.Authentication.Basic.Password
	}

	var pathUrl string
	var found bool

	if pathUrl, found = r.config.Endpoints["getFileInfo"]; !found {
		err := errors.New("endpoint getFileInfo not found")
		return nil, r.config.Url, err
	}
	params := "?refId=" + req.RefID + "&" + "orderId=" + req.OrderId + "&" + "applicationCode=" + req.ApplicationCode
	url := r.config.Url + pathUrl
	url = url + params
	logStepRequest.Endpoint = url

	d, code, err := utils.Get(url, &headers, nil, &options)
	if err != nil {
		return nil, r.config.Url, err
	}

	if code != 200 {
		err = errors.New("StatusCode err from 3rd party " + strconv.Itoa(code))
		logStepRequest.Response = string(d)
		logStepRequest.ResultCode = fmt.Sprint(code)
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, r.config.Url, err
	}

	var response GetFileInfoResponse
	if err := json.Unmarshal(d, &response); err != nil {
		logStepRequest.Response = string(d)
		logStepRequest.ResultCode = "500"
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, r.config.Url, err
	}
	resByte, _ := json.Marshal(response)
	logStepRequest.StepResponse = string(resByte)
	logStepRequest.ResultCode = "200"
	logStepRequest.ResultDesc = "Success"
	logger.LogStepPool(logStepRequest, logModel, startStep)
	return &response, r.config.Url, nil
}

func (r *CallGetFileInfo) GetDocumentByOrderId(req GetFileInfoRequest, logModel logger.LogModel) (*GetFileInfoResponse, string, error) {

	startStep := time.Now()
	reqByte, _ := json.Marshal(req)

	logStepRequest := logger.LogStepRequest{
		StepName:    "Doc System - GetDocumentByOrderId",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    r.config.Url,
		Method:      "GET",
		System:      "Doc System",
	}

	headers := map[string]string{}
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	headers["Content-Type"] = "application/json"

	var pathUrl string
	var found bool

	if pathUrl, found = r.config.Endpoints["getFileInfo"]; !found {
		err := errors.New("endpoint getFileInfo not found")
		return nil, r.config.Url, err
	}
	logStepRequest.Endpoint += pathUrl
	url := r.config.Url + pathUrl
	url = url + fmt.Sprintf("?orderId={%s}&applicationCode={%s}", req.OrderId, req.ApplicationCode)

	d, code, err := utils.Get(url, &headers, nil, &options)
	if err != nil {
		return nil, r.config.Url, err
	}

	if code != 200 {
		err = errors.New("StatusCode err from 3rd party " + strconv.Itoa(code))
		logStepRequest.Response = string(d)
		logStepRequest.ResultCode = fmt.Sprint(code)
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, r.config.Url, err
	}

	var response GetFileInfoResponse
	if err := json.Unmarshal(d, &response); err != nil {
		logStepRequest.Response = string(d)
		logStepRequest.ResultCode = "500"
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, r.config.Url, err
	}
	resByte, _ := json.Marshal(response)
	logStepRequest.StepResponse = string(resByte)
	logStepRequest.ResultCode = "200"
	logStepRequest.ResultDesc = "Success"
	logger.LogStepPool(logStepRequest, logModel, startStep)
	return &response, r.config.Url, nil
}
