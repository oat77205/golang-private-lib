package getfileinfo

type GetFileInfoRequest struct {
	RefID           string `json:"refId"`
	OrderId         string `json:"orderId"`
	ApplicationCode string `json:"applicationCode"`
}

type GetFileInfoResponse struct {
	Code     string `json:"code"`
	BizError string `json:"bizError"`
	Message  string `json:"message"`
	Data     Data   `json:"data"`
}

type Data struct {
	Document []Document `json:"document"`
}
type Document struct {
	Id              string `json:"id"`
	RefId           string `json:"refId"`
	OrderId         string `json:"orderId"`
	ApplicationCode string `json:"applicationCode"`
	TempETag        string `json:"tempETag"`
	Dize            string `json:"size"`
	TempLocation    string `json:"tempLocation"`
	DocumentType    string `json:"documentType"`
	Mimetype        string `json:"mimetype"`
	TruedocRefId    string `json:"truedocRefId"`
	TruedocStatus   string `json:"truedocStatus"`
}
