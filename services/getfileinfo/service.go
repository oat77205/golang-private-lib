package getfileinfo

import (
	"encoding/json"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceGetFileInfo interface {
	GetFileInfoService(GetFileInfoRequest, logger.LogModel) (*GetFileInfoResponse, error)
	GetFileInfoServiceByOrderId(GetFileInfoRequest, logger.LogModel) (*GetFileInfoResponse, error)
}

type serviceGetFileInfo struct {
	GetFileInfo ICallGetFileInfo
}

func NewServiceGetFileInfo(GetFileInfo ICallGetFileInfo) IServiceGetFileInfo {
	return &serviceGetFileInfo{
		GetFileInfo: GetFileInfo,
	}
}

func (r *serviceGetFileInfo) GetFileInfoService(req GetFileInfoRequest, logModel logger.LogModel) (*GetFileInfoResponse, error) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, err := r.GetFileInfo.GetDocument(req, logModel)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = resp.Code
			statusDescription = resp.Message
			if statusCode == "200" {
				statusCode = "200"
				statusDescription = "Success"
			} else {
				statusCode = "900"
				statusDescription = "GetFileInfo fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}

	reqByte, _ := json.Marshal(req)
	resByte, _ := json.Marshal(resp)

	logStepRequest := logger.LogStepRequest{
		StepName:     "HL-OTP - GetFileInfo",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(reqByte),
		StepResponse: string(resByte),
		Endpoint:     url,
		Method:       "PATCH",
		System:       "",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return resp, err
}

func (r *serviceGetFileInfo) GetFileInfoServiceByOrderId(req GetFileInfoRequest, logModel logger.LogModel) (*GetFileInfoResponse, error) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, err := r.GetFileInfo.GetDocumentByOrderId(req, logModel)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = resp.Code
			statusDescription = resp.Message
			if statusCode == "200" {
				statusCode = "200"
				statusDescription = "Success"
			} else {
				statusCode = "900"
				statusDescription = "GetFileInfo fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}

	reqByte, _ := json.Marshal(req)
	resByte, _ := json.Marshal(resp)

	logStepRequest := logger.LogStepRequest{
		StepName:     "HL-OTP - GetFileInfo",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(reqByte),
		StepResponse: string(resByte),
		Endpoint:     url,
		Method:       "PATCH",
		System:       "",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return resp, err
}
