package claim

type ClaimRequest struct {
	FullName           string   `json:"fullName"`
	Date               string   `json:"date"`
	IDNumber           string   `json:"idNumber"`
	FinalPrice         int      `json:"finalPrice"`
	AdvancePayment     int      `json:"advancePayment"`
	ExtraAdvanceAmount int      `json:"extraAdvanceAmount"`
	ProductPrice       int      `json:"productPrice"`
	PricePlan          string   `json:"pricePlan"`
	ContractTerm       string   `json:"contractTerm"`
	IsResume           bool     `json:"isResume"`
	Type               string   `json:"type"`
	Msisdn             string   `json:"msisdn"`
	CitizenBase64      string   `json:"citizenBase64"`
	Watermark          string   `json:"watermark"`
	MaxSizeMB          string   `json:"maxSizeMB"`
	ModelDesc          string   `json:"modelDesc"`
	IDType             string   `json:"idType"`
	Signature          string   `json:"signature"`
	Customer           Customer `json:"customer"`
}

type Customer struct {
	IDNumber     string `json:"idNumber"`
	TitleTh      string `json:"titleTh"`
	FirstNameTh  string `json:"firstNameTh"`
	MiddleNameTh string `json:"middleNameTh"`
	LastNameTh   string `json:"lastNameTh"`
	TitleEn      string `json:"titleEn"`
	FirstNameEn  string `json:"firstNameEn"`
	MiddleNameEn string `json:"middleNameEn"`
	LastNameEn   string `json:"lastNameEn"`
	BirthDate    string `json:"birthDate"`
	Moo          string `json:"moo"`
	Soi          string `json:"soi"`
	Trok         string `json:"trok"`
	Road         string `json:"road"`
	Subdistrict  string `json:"subdistrict"`
	District     string `json:"district"`
	Province     string `json:"province"`
	IssueDate    string `json:"issueDate"`
	ExpireDate   string `json:"expireDate"`
	Photo        string `json:"photo"`
}

type ClaimResponse struct {
	File []byte `json:"file"`
}
