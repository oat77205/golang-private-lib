package claim

import (
	"encoding/json"
	"errors"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type ICallClaim interface {
	Claim(req ClaimRequest, logModel logger.LogModel) (*utils.ResponseStandard, string, int, error)
}

type CallClaim struct {
	config utils.EtcdApiConfig
}

func NewCallClaim(conf utils.EtcdApiConfig) ICallClaim {
	return &CallClaim{
		config: conf,
	}
}

func (r *CallClaim) Claim(req ClaimRequest, logModel logger.LogModel) (*utils.ResponseStandard, string, int, error) {
	var pathUrl string
	var found bool
	var url string
	headers := map[string]string{}
	headers["api-key"] = r.config.Authentication.ApiKey
	requestJSON, _ := json.Marshal(req)
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	endpoint := "claim"
	if pathUrl, found = r.config.Endpoints[endpoint]; !found {
		return nil, url, 404, errors.New("endpoint " + endpoint + " not found")
	}

	url = r.config.Url + pathUrl
	d, code, err := utils.Get(url, &headers, requestJSON, &options)
	if err != nil {
		return nil, url, code, err
	}

	response := new(utils.ResponseStandard)
	if err := json.Unmarshal(d, &response); err != nil {
		if response.Code == "" {
			response.Data = d
			return response, url, code, nil
		}
		return nil, url, code, err
	}
	return response, url, code, nil
}
