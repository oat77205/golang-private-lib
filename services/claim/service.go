package claim

import (
	"encoding/json"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceClaim interface {
	ClaimService(req ClaimRequest, logModel logger.LogModel) (utils.Result, *utils.ResponseStandard)
}

type serviceClaim struct {
	Claim ICallClaim
}

func NewServiceApp4in1(claim ICallClaim) IServiceClaim {
	return &serviceClaim{
		Claim: claim,
	}
}

func (r *serviceClaim) ClaimService(req ClaimRequest, logModel logger.LogModel) (utils.Result, *utils.ResponseStandard) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, code, err := r.Claim.Claim(req, logModel)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = "200"
			statusDescription = "Success"
			if code != 200 {
				statusCode = "900"
				statusDescription = resp.Message
				statusDescription = "Report App4in1 fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}

	reqByte, _ := json.Marshal(req)
	resByte, _ := json.Marshal(resp)

	logStepRequest := logger.LogStepRequest{
		StepName:     "Report - Claim",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(reqByte),
		StepResponse: string(resByte),
		Endpoint:     url,
		Method:       "GET",
		System:       "",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return utils.Result{Code: statusCode, Description: statusDescription}, resp
}
