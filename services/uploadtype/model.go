package uploadtype

type UploadTypeRequest struct {
	CorrelationID   string `json:"correlationId"`
	Channel         string `json:"channel"`
	UserID          string `json:"userId"`
	RefID           string `json:"refId"`
	OrderID         string `json:"orderId"`
	ApplicationCode string `json:"applicationCode"`
	UploadType      string `json:"uploadType"`
}

type UploadTypeResponse struct {
	Code     string           `json:"code"`
	BizError string           `json:"bizError"`
	Message  string           `json:"message"`
	Data     []UploadTypeData `json:"data"`
}

type UploadTypeData struct {
	RefID    string   `json:"refId"`
	Document Document `json:"document"`
}

type Document struct {
	TempETag      string `json:"tempETag"`
	Size          string `json:"size"`
	TempLocation  string `json:"tempLocation"`
	DocumentType  string `json:"documentType"`
	FileType      string `json:"fileType"`
	TruedocRefID  string `json:"truedocRefId"`
	TruedocStatus string `json:"truedocStatus"`
}
