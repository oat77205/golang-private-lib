package uploadtype

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type ICallUploadType interface {
	UploadType(UploadTypeRequest, logger.LogModel, string) (*UploadTypeResponse, string, error)
}

type CallUploadType struct {
	config utils.EtcdApiConfig
}

func NewCallUploadType(conf utils.EtcdApiConfig) ICallUploadType {
	return &CallUploadType{
		config: conf,
	}
}

func (r *CallUploadType) UploadType(req UploadTypeRequest, logModel logger.LogModel, stepName string) (*UploadTypeResponse, string, error) {
	requestJSON, _ := json.Marshal(req)

	if len(stepName) == 0 {
		stepName = "OMNI - UploadType"
	}

	headers := map[string]string{}
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	if r.config.Authentication.Basic.Password != "" {
		headers["apiKey"] = r.config.Authentication.Basic.Password
	}
	headers["Content-Type"] = "application/json"
	headers["correlationId"] = utils.GetUUID()

	var pathUrl string
	var found bool

	if pathUrl, found = r.config.Endpoints["uploadType"]; !found {
		err := errors.New("endpoint uploadType not found")
		return nil, "", err
	}
	url := r.config.Url + pathUrl

	d, statusCode, _, err := callHttp(logModel.Txid, url, &headers, requestJSON, http.MethodPatch, &options)

	if err != nil {
		return nil, url, err
	}
	if statusCode != 200 {
		err = errors.New("StatusCode err from 3rd party " + strconv.Itoa(statusCode))
		return nil, url, err
	}

	var response UploadTypeResponse
	if err := json.Unmarshal(d, &response); err != nil {
		return nil, url, err
	}
	return &response, url, nil
}

func callHttp(tranID, uri string, header *map[string]string, body []byte, method string, op *utils.RequestOption) (rawBody []byte, statusCode int, respHeader http.Header, err error) {
	// startProcess := time.Now()
	client := http.Client{
		Timeout: time.Duration(op.Timeout) * time.Second,
		Transport: &http.Transport{TLSClientConfig: &tls.Config{
			InsecureSkipVerify: op.InsecureSkipVerify,
		}, DisableKeepAlives: true},
	}
	buff := bytes.NewBuffer(body)
	req, err := http.NewRequest(method, uri, buff)
	// s := newrelic.StartExternalSegment(txn, req)
	// defer s.End()
	if err != nil {
		// s.AddAttribute("errMsg", err.Error())
		return
	}
	if len(*header) > 0 {
		for key, value := range *header {
			req.Header.Add(key, value)
		}
	}

	if (*header)["Content-Type"] != "" {
		req.Header.Add("Content-Type", (*header)["Content-Type"])
	} else {
		req.Header.Add("Content-Type", "application/json")
	}

	resp, err := client.Do(req)
	// s.AddAttribute("tranId", tranID)
	// s.Response = resp

	if err != nil {
		// s.AddAttribute("errMsg", err.Error())
		return
	}
	respHeader = resp.Header
	statusCode = resp.StatusCode
	defer resp.Body.Close()
	rawBody, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		// s.AddAttribute("errMsg", err.Error())
		return
	}
	// log.Debug("End Call Restful API :: elapsed %v ms | statusCode : %d", log.GetElapsedTime(startProcess), statusCode)
	return
}
