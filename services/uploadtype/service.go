package uploadtype

import (
	"encoding/json"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceUploadType interface {
	UploadTypeService(UploadTypeRequest, logger.LogModel, string) (*UploadTypeResponse, error)
}

type serviceUploadType struct {
	UploadType ICallUploadType
}

func NewServiceUploadType(UploadType ICallUploadType) IServiceUploadType {
	return &serviceUploadType{
		UploadType: UploadType,
	}
}

func (r *serviceUploadType) UploadTypeService(req UploadTypeRequest, logModel logger.LogModel, stepName string) (*UploadTypeResponse, error) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, err := r.UploadType.UploadType(req, logModel, stepName)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = resp.Code
			statusDescription = resp.Message
			if statusCode == "200" {
				statusCode = "200"
				statusDescription = "Success"
			} else {
				statusCode = "900"
				statusDescription = "UploadType fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}

	reqByte, _ := json.Marshal(req)
	resByte, _ := json.Marshal(resp)

	if len(stepName) == 0 {
		stepName = "OMNI - UploadType"
	}

	logStepRequest := logger.LogStepRequest{
		StepName:     stepName,
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(reqByte),
		StepResponse: string(resByte),
		Endpoint:     url,
		Method:       "PATCH",
		System:       "OMNI",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return resp, err
}
