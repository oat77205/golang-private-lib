package app4in1

import (
	"encoding/json"
	"errors"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type ICallApp4in1 interface {
	App4in1(req App4in1Request, logModel logger.LogModel) (*utils.ResponseStandard, string, int, error)
}

type CallApp4in1 struct {
	config utils.EtcdApiConfig
}

func NewCallApp4in1(conf utils.EtcdApiConfig) ICallApp4in1 {
	return &CallApp4in1{
		config: conf,
	}
}

func (r *CallApp4in1) App4in1(req App4in1Request, logModel logger.LogModel) (*utils.ResponseStandard, string, int, error) {
	var pathUrl string
	var found bool
	var url string
	headers := map[string]string{}
	headers["api-key"] = r.config.Authentication.ApiKey
	requestJSON, _ := json.Marshal(req)
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	endpoint := "app4in1"
	if pathUrl, found = r.config.Endpoints[endpoint]; !found {
		return nil, url, 404, errors.New("endpoint " + endpoint + " not found")
	}

	url = r.config.Url + pathUrl
	d, code, err := utils.Get(url, &headers, requestJSON, &options)
	if err != nil {
		return nil, url, code, err
	}

	response := new(utils.ResponseStandard)
	if err := json.Unmarshal(d, &response); err != nil {
		if response.Code == "" {
			response.Data = d
			return response, url, code, nil
		}
		return nil, url, code, err
	}
	return response, url, code, nil
}
