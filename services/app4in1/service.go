package app4in1

import (
	"encoding/json"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceApp4in1 interface {
	App4in1Service(req App4in1Request, logModel logger.LogModel) (utils.Result, *utils.ResponseStandard)
}

type serviceApp4in1 struct {
	App4in1 ICallApp4in1
}

func NewServiceApp4in1(app4in1 ICallApp4in1) IServiceApp4in1 {
	return &serviceApp4in1{
		App4in1: app4in1,
	}
}

func (r *serviceApp4in1) App4in1Service(req App4in1Request, logModel logger.LogModel) (utils.Result, *utils.ResponseStandard) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, code, err := r.App4in1.App4in1(req, logModel)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = "200"
			statusDescription = "Success"
			if code != 200 {
				statusCode = "900"
				statusDescription = resp.Message
				statusDescription = "Report App4in1 fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}

	reqByte, _ := json.Marshal(req)
	resByte, _ := json.Marshal(resp)

	logStepRequest := logger.LogStepRequest{
		StepName:     "Report - App4in1",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(reqByte),
		StepResponse: string(resByte),
		Endpoint:     url,
		Method:       "GET",
		System:       "",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return utils.Result{Code: statusCode, Description: statusDescription}, resp
}
