package app4in1

type App4in1Request struct {
	IsResume       bool           `json:"isResume"`
	Type           string         `json:"type"`
	Watermark      string         `json:"watermark"`
	MaxSizeMB      string         `json:"maxSizeMB"`
	Date           string         `json:"date"`
	OrderID        string         `json:"orderId"`
	FullName       string         `json:"fullName"`
	TitleName      string         `json:"titleName"`
	Documents      string         `json:"documents"`
	IDNumber       string         `json:"idNumber"`
	DobFormatTh    string         `json:"dobFormatTh"`
	ExpireDay      string         `json:"expireDay"`
	Status         string         `json:"status"`
	Nationality    string         `json:"nationality"`
	Job            string         `json:"job"`
	WorkYear       string         `json:"workYear"`
	WorkMonth      string         `json:"workMonth"`
	ContactNo      string         `json:"contactNo"`
	Email          string         `json:"email"`
	SameAddress    bool           `json:"sameAddress"`
	PdpaConsent    bool           `json:"pdpaConsent"`
	TmnConsent     bool           `json:"tmnConsent"`
	StaffNo        string         `json:"staffNo"`
	DealerNo       string         `json:"dealerNo"`
	DealerName     string         `json:"dealerName"`
	Address        Address        `json:"address"`
	BillingAddress BillingAddress `json:"billingAddress"`
	TrueOnline     TrueOnline     `json:"trueOnline,omitempty"`
	TrueVision     TrueVision     `json:"trueVision,omitempty"`
	TrueMoveH      TrueMoveH      `json:"trueMoveH,omitempty"`
	Customer       Customer       `json:"customer"`
	Signature      string         `json:"signature"`
	CitizenBase64  string         `json:"citizenBase64"`
}

type Address struct {
	No          string `json:"no"`
	Building    string `json:"building"`
	Swine       string `json:"swine"`
	Alley       string `json:"alley"`
	Road        string `json:"road"`
	Subdistrict string `json:"subdistrict"`
	District    string `json:"district"`
	Province    string `json:"province"`
	PostalCode  string `json:"postalCode"`
}

type BillingAddress struct {
	No          string `json:"no"`
	Building    string `json:"building"`
	Swine       string `json:"swine"`
	Alley       string `json:"alley"`
	Road        string `json:"road"`
	Subdistrict string `json:"subdistrict"`
	District    string `json:"district"`
	Province    string `json:"province"`
	PostalCode  string `json:"postalCode"`
}

type TrueOnline struct {
	IsNew           bool    `json:"isNew,omitempty"`
	IsInternet      bool    `json:"isInternet,omitempty"`
	InternetDetail  string  `json:"internetDetail,omitempty"`
	IsHomePhone     bool    `json:"isHomePhone,omitempty"`
	HomePhoneDetail string  `json:"homePhoneDetail,omitempty"`
	ServiceDetail   string  `json:"serviceDetail,omitempty"`
	PayMonthAmount  float64 `json:"payMonthAmount,omitempty"`
	InstallDate     string  `json:"installDate,omitempty"`
	IncomingPrice   int     `json:"incomingPrice,omitempty"`
	MonthPay        int     `json:"monthPay,omitempty"`
}

type TrueVision struct {
	IsNew             bool   `json:"isNew,omitempty"`
	MemberNo          string `json:"memberNo,omitempty"`
	Package           string `json:"package,omitempty"`
	AdditionalPackage string `json:"additionalPackage,omitempty"`
	AddPoint          int    `json:"addPoint,omitempty"`
	InstallDate       string `json:"installDate,omitempty"`
	MonthPay          int    `json:"monthPay,omitempty"`
}

type TrueMoveH struct {
	IsChange       bool   `json:"isChange,omitempty"`
	IsNew          bool   `json:"isNew,omitempty"`
	Msisdn         string `json:"msisdn,omitempty"`
	ChangeMobileNo string `json:"changeMobileNo,omitempty"`
	ChangeRefNo    string `json:"changeRefNo,omitempty"`
	ChangeFrom     string `json:"changeFrom,omitempty"`
	Package        string `json:"package,omitempty"`
	MonthPay       int    `json:"monthPay,omitempty"`
}

type Customer struct {
	IDNumber     string `json:"idNumber"`
	TitleTh      string `json:"titleTh"`
	FirstNameTh  string `json:"firstNameTh"`
	MiddleNameTh string `json:"middleNameTh"`
	LastNameTh   string `json:"lastNameTh"`
	TitleEn      string `json:"titleEn"`
	FirstNameEn  string `json:"firstNameEn"`
	MiddleNameEn string `json:"middleNameEn"`
	LastNameEn   string `json:"lastNameEn"`
	BirthDate    string `json:"birthDate"`
	Moo          string `json:"moo"`
	Soi          string `json:"soi"`
	Trok         string `json:"trok"`
	Road         string `json:"road"`
	Subdistrict  string `json:"subdistrict"`
	District     string `json:"district"`
	Province     string `json:"province"`
	IssueDate    string `json:"issueDate"`
	ExpireDate   string `json:"expireDate"`
	Photo        string `json:"photo"`
}

type App4in1Response struct {
	File []byte `json:"file"`
}
