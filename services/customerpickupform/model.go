package customerpickupform

type CustomerPickupFormRequest struct {
	Watermark         string          `json:"watermark"`
	Signature         string          `json:"signature"`
	CustomerFullName  string          `json:"customerFullName"`
	CustomerContactNo string          `json:"customerContactNo"`
	OrderNumber       string          `json:"orderNumber"`
	Channel           string          `json:"channel"`
	PickupDate        string          `json:"pickupDate"`
	TargetDealerName  string          `json:"targetDealerName"`
	PickupFullname    string          `json:"pickupFullname"`
	PickupNo          string          `json:"pickupNo"`
	OrderItemList     []OrderItemList `json:"orderItemList"`
	TrackingIDList    []string        `json:"trackingIdList"`
	IsPickup          bool            `json:"isPickup"`
	PickupReason      string          `json:"pickupReason"`
	UserID            string          `json:"userId"`
	UserFullName      string          `json:"userFullName"`
}

type OrderItemList struct {
	ItemDescription string `json:"itemDescription"`
	Qty             int    `json:"qty"`
}

type CustomerPickupFormResponse struct {
	Code     string `json:"code"`
	BizError string `json:"bizError"`
	Message  string `json:"message"`
	Data     string `json:"data"`
}
