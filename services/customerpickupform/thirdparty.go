package customerpickupform

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type ICallCustomerPickupForm interface {
	GeneratePickupForm(CustomerPickupFormRequest, logger.LogModel, string) (*CustomerPickupFormResponse, string, error)
}

type CallCustomerPickupForm struct {
	config utils.EtcdApiConfig
}

func NewCallCustomerPickupForm(conf utils.EtcdApiConfig) ICallCustomerPickupForm {
	return &CallCustomerPickupForm{
		config: conf,
	}
}

func (r *CallCustomerPickupForm) GeneratePickupForm(req CustomerPickupFormRequest, logModel logger.LogModel, token string) (*CustomerPickupFormResponse, string, error) {

	startStep := time.Now()
	reqByte, _ := json.Marshal(req)

	logStepRequest := logger.LogStepRequest{
		StepName:    "Doc System - GeneratePickupForm",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    r.config.Url,
		Method:      "POST",
		System:      "Doc System",
	}

	headers := map[string]string{}
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	headers["Content-Type"] = "application/json"

	headers["Authorization"] = token

	var pathUrl string
	var found bool

	if pathUrl, found = r.config.Endpoints["customerPickupForm"]; !found {
		err := errors.New("endpoint customerPickupForm not found")
		return nil, r.config.Url, err
	}
	url := r.config.Url + pathUrl
	logStepRequest.Endpoint = url

	resp, statusCode, _, err := utils.PostHttp(logModel.Txid, url, &headers, reqByte, &options)
	if err != nil {
		return nil, url, err
	}
	if statusCode != 200 && statusCode != 201 {
		err = errors.New("StatusCode err from 3rd party " + strconv.Itoa(statusCode))
		logStepRequest.Response = string(resp)
		logStepRequest.ResultCode = fmt.Sprint(statusCode)
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, url, err
	}

	var response CustomerPickupFormResponse
	if resp == nil {
		logStepRequest.Response = string(resp)
		logStepRequest.ResultCode = "500"
		logStepRequest.ResultDesc = err.Error()
		logger.LogStepPool(logStepRequest, logModel, startStep)
		return nil, url, err
	}
	response.Code = "200"
	response.Message = "Success"
	response.BizError = ""
	base64Resp := base64.StdEncoding.EncodeToString(resp)
	response.Data = base64Resp

	resByte, _ := json.Marshal(response)
	logStepRequest.StepResponse = string(resByte)
	logStepRequest.ResultCode = "200"
	logStepRequest.ResultDesc = "Success"
	logger.LogStepPool(logStepRequest, logModel, startStep)
	return &response, url, nil
}
