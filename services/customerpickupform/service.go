package customerpickupform

import (
	"encoding/json"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceCustomerPickupForm interface {
	CustomerPickupFormService(CustomerPickupFormRequest, logger.LogModel, string) (*CustomerPickupFormResponse, error)
}

type serviceCustomerPickupForm struct {
	CustomerPickupForm ICallCustomerPickupForm
}

func NewServiceCustomerPickupForm(CustomerPickupForm ICallCustomerPickupForm) IServiceCustomerPickupForm {
	return &serviceCustomerPickupForm{
		CustomerPickupForm: CustomerPickupForm,
	}
}

func (r *serviceCustomerPickupForm) CustomerPickupFormService(req CustomerPickupFormRequest, logModel logger.LogModel, token string) (*CustomerPickupFormResponse, error) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, err := r.CustomerPickupForm.GeneratePickupForm(req, logModel, token)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = resp.Code
			statusDescription = resp.Message
			if statusCode == "200" {
				statusCode = "200"
				statusDescription = "Success"
			} else {
				statusCode = "900"
				statusDescription = "CustomerPickupForm fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}

	reqByte, _ := json.Marshal(req)
	resByte, _ := json.Marshal(resp)

	logStepRequest := logger.LogStepRequest{
		StepName:     "HL-OTP - CustomerPickupForm",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(reqByte),
		StepResponse: string(resByte),
		Endpoint:     url,
		Method:       "POST",
		System:       "",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return resp, err
}
