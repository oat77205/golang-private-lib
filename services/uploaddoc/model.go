package uploaddoc

type UploadDocRequest struct {
	CorrelationID   string         `json:"correlationId"`
	Channel         string         `json:"channel"`
	UserID          string         `json:"userId"`
	OrderID         string         `json:"orderId"`
	UploadType      string         `json:"uploadType"`
	SubmitDate      string         `json:"submitDate"`
	FileBase64      string         `json:"fileBase64"`
	FileName        string         `json:"fileName"`
	FileType        string         `json:"fileType"`
	ApplicationCode string         `json:"applicationCode"`
	MimeType        string         `json:"mimeType"`
	Destination     string         `json:"destination"`
	Overwrite       string         `json:"overwrite"`
	IdCard          string         `json:"idCard"`
	IdType          string         `json:"idType"`
	Firstname       string         `json:"firstname"`
	Lastname        string         `json:"lastname"`
	BookbankNo      string         `json:"bookbankNo"`
	BankName        string         `json:"bankName"`
	Tvsno           string         `json:"tvsno"`
	AppFormNo       string         `json:"appFormNo"`
	AdditionalData  AdditionalData `json:"additionalData"`
}

type AdditionalData struct {
	MobileNo  string `json:"mobileNo"`
	ShopCode  string `json:"shopCode"`
	Product   string `json:"product"`
	CircuitNo string `json:"circuitNo"`
}
type UploadDocResponse struct {
	Code     string        `json:"code"`
	Message  string        `json:"message"`
	Error    []Error       `json:"error,omitempty"`
	BizError string        `json:"bizError"`
	File     string        `json:"file,omitempty"`
	Data     UploadDocData `json:"data,omitempty"`
}

type Error struct {
	Param   string `json:"param"`
	Message string `json:"message"`
}

type UploadDocData struct {
	RefID    string   `json:"refId"`
	Document Document `json:"document"`
}

type Document struct {
	TempETag      string `json:"tempETag"`
	Size          string `json:"size"`
	TempLocation  string `json:"tempLocation"`
	DocumentType  string `json:"documentType"`
	FileType      string `json:"fileType"`
	TruedocRefID  string `json:"truedocRefId"`
	TruedocStatus string `json:"truedocStatus"`
}
