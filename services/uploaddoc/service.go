package uploaddoc

import (
	"encoding/json"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceUploadDoc interface {
	UploadDocService(UploadDocRequest, logger.LogModel, string) (*UploadDocResponse, error)
	UploadDocService1Log(req UploadDocRequest, logModel logger.LogModel, stepName string) (utils.Result, *UploadDocResponse)
}

type serviceUploadDoc struct {
	UploadDoc ICallUploadDoc
}

func NewServiceUploadDoc(UploadDoc ICallUploadDoc) IServiceUploadDoc {
	return &serviceUploadDoc{
		UploadDoc: UploadDoc,
	}
}

func (r *serviceUploadDoc) UploadDocService(req UploadDocRequest, logModel logger.LogModel, stepName string) (*UploadDocResponse, error) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, err := r.UploadDoc.UploadDoc(req, logModel, stepName)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = resp.Code
			statusDescription = resp.Message
			if statusCode == "200" {
				statusCode = "200"
				statusDescription = "Success"
			} else {
				statusCode = "900"
				statusDescription = "UploadDoc fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}

	reqByte, _ := json.Marshal(req)
	resByte, _ := json.Marshal(resp)

	var logRequest UploadDocRequest
	json.Unmarshal(reqByte, &logRequest)
	logRequest.FileBase64 = ""

	logRequestByte, _ := json.Marshal(logRequest)

	if len(stepName) == 0 {
		stepName = "DOC-SYSTEM - UploadDoc"
	}

	logStepRequest := logger.LogStepRequest{
		StepName:     stepName,
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(logRequestByte),
		StepResponse: string(resByte),
		Endpoint:     url,
		Method:       "POST",
		System:       "DOC-SYSTEM",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)

	return resp, err
}

func (r *serviceUploadDoc) UploadDocService1Log(req UploadDocRequest, logModel logger.LogModel, stepName string) (utils.Result, *UploadDocResponse) {
	startStep := time.Now()
	statusCode := ""
	statusDescription := ""

	resp, url, err := r.UploadDoc.UploadDoc(req, logModel, stepName)
	if err != nil {
		statusCode = "500"
		statusDescription = err.Error()
	} else {
		if resp != nil {
			statusCode = resp.Code
			statusDescription = resp.Message
			if statusCode == "200" {
				statusCode = "200"
				statusDescription = "Success"
			} else {
				statusCode = "900"
				statusDescription = "UploadDoc fail [code= " + statusCode + ", description= " + statusDescription + "]"
			}
		}
	}
	reqByte, _ := json.Marshal(req)
	resByte, _ := json.Marshal(resp)
	var logRequest UploadDocRequest
	json.Unmarshal(reqByte, &logRequest)
	logRequest.FileBase64 = ""
	if len(stepName) == 0 {
		stepName = "DOC-SYSTEM - UploadDoc"
	}
	logRequestByte, _ := json.Marshal(logRequest)
	logStepRequest := logger.LogStepRequest{
		StepName:     stepName,
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  string(logRequestByte),
		StepResponse: string(resByte),
		Endpoint:     url,
		Method:       "POST",
		System:       "DOC-SYSTEM",
		ResultCode:   statusCode,
		ResultDesc:   statusDescription,
	}

	logger.LogStepPool(logStepRequest, logModel, startStep)
	return utils.Result{Code: statusCode, Description: statusDescription}, resp
}
