package uploaddoc

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io"
	"mime/multipart"
	"strconv"
	"strings"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
)

type ICallUploadDoc interface {
	UploadDoc(UploadDocRequest, logger.LogModel, string) (*UploadDocResponse, string, error)
}

type CallUploadDoc struct {
	config utils.EtcdApiConfig
}

func NewCallUploadDoc(conf utils.EtcdApiConfig) ICallUploadDoc {
	return &CallUploadDoc{
		config: conf,
	}
}

func (r *CallUploadDoc) UploadDoc(req UploadDocRequest, logModel logger.LogModel, stepName string) (*UploadDocResponse, string, error) {

	headers := map[string]string{}
	options := utils.RequestOption{
		Timeout:            r.config.Timeout,
		InsecureSkipVerify: r.config.InsecureSkipVerify,
	}

	if r.config.Authentication.Basic.Password != "" {
		headers["apiKey"] = r.config.Authentication.Basic.Password
	}

	headers["correlationId"] = utils.GetUUID()

	var pathUrl string
	var found bool

	if pathUrl, found = r.config.Endpoints["uploadDoc"]; !found {
		err := errors.New("endpoint uploadDoc not found")
		return nil, "", err
	}

	url := r.config.Url + pathUrl

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	defer writer.Close()

	writer = AddField(writer, "correlationId", req.CorrelationID)
	writer = AddField(writer, "channel", req.Channel)
	writer = AddField(writer, "userId", req.UserID)
	writer = AddField(writer, "uploadType", req.UploadType)
	writer = AddField(writer, "orderId", req.OrderID)
	writer = AddField(writer, "submitDate", req.SubmitDate)

	writer = AddField(writer, "filename", req.FileName)
	writer = AddField(writer, "type", req.FileType)
	writer = AddField(writer, "applicationCode", req.ApplicationCode)
	writer = AddField(writer, "destination", req.Destination)
	writer = AddField(writer, "mimetype", req.MimeType)
	writer = AddField(writer, "overwrite", req.Overwrite)

	writer = AddField(writer, "idCard", req.IdCard)
	writer = AddField(writer, "idType", req.IdType)
	writer = AddField(writer, "firstname", req.Firstname)
	writer = AddField(writer, "lastname", req.Lastname)
	writer = AddField(writer, "bookbankNo", req.BookbankNo)
	writer = AddField(writer, "bankName", req.BankName)
	writer = AddField(writer, "tvsno", req.Tvsno)
	writer = AddField(writer, "appFormNo", req.AppFormNo)

	writer = AddField(writer, "additionalData.mobileNo", req.AdditionalData.MobileNo)
	writer = AddField(writer, "additionalData.shopCode", req.AdditionalData.ShopCode)
	writer = AddField(writer, "additionalData.product", req.AdditionalData.Product)
	writer = AddField(writer, "additionalData.circuitNo", req.AdditionalData.CircuitNo)

	dec := base64.NewDecoder(base64.StdEncoding, strings.NewReader(req.FileBase64))

	part, err := writer.CreateFormFile("file", req.FileName)

	io.Copy(part, dec)
	writer.Close()
	if err != nil {
		return nil, url, err
	}

	headers["Content-Type"] = writer.FormDataContentType()

	d, statusCode, _, err := utils.PostHttp(logModel.Txid, url, &headers, body.Bytes(), &options)

	if err != nil {
		return nil, url, err
	}

	if statusCode != 200 && statusCode != 201 {
		err = errors.New("StatusCode err from 3rd party " + strconv.Itoa(statusCode))
		return nil, url, err
	}

	var response UploadDocResponse
	if err := json.Unmarshal(d, &response); err != nil {
		return nil, url, err
	}
	return &response, url, nil
}

func AddField(writer *multipart.Writer, name string, value string) *multipart.Writer {
	fw, _ := writer.CreateFormField(name)
	_, _ = io.Copy(fw, strings.NewReader(value))
	return writer
}
