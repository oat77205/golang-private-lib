package models

type Envconfig struct {
	DbConnection string
	Template     ApiConfig
}

type ApiConfig struct {
	Url                string
	InsecureSkipVerify bool
	Timeout            int
	ApiKey             string
	Username           string
	Password           string
}

//For Query common config
type CommonConfigStruct struct {
	RuleName      string      `json:"ruleName" bson:"ruleName"`
	ActionList    []string    `json:"actionList,omitempty" bson:"actionList,omitempty"`
	RuleValueList []RuleValue `json:"ruleValueList,omitempty" bson:"ruleValueList,omitempty"`
}

type RuleValue struct {
	RuleId       int32      `json:"ruleId,omitempty" bson:"ruleId,omitempty"`
	RuleItemList []RuleItem `json:"ruleItem,omitempty" bson:"ruleItem,omitempty"`
}

type RuleItem struct {
	Type      string   `json:"type,omitempty" bson:"type,omitempty"`
	ValueList []string `json:"valueList,omitempty" bson:"valueList,omitempty"`
	Oper      string   `json:"oper,omitempty" bson:"oper,omitempty"`
}

type GetConfigRequest struct {
	RuleName     string `json:"ruleName" validate:"required"`
	ActivityName string `json:"activityName,omitempty"`
	RuleId       int64  `json:"ruleId,omitempty"`
	Type         string `json:"type,omitempty"`
	Value        string `json:"value,omitempty"`
}

//For other config//
type UnCommonStruct struct {
	Rulename    string    `json:"ruleName" bson:"ruleName"`
	PurposeList []Purpose `json:"purposeList,omitempty" bson:"purposeList,omitempty"`
	// CollectionList           []Collection      `json:"collectionList,omitempty" bson:"collectionList,omitempty"`
	// SMSList                  *SMS              `json:"sms,omitempty" bson:"sms,omitempty"`
	// ActivateOptionList       []ActivateOption  `json:"activateOption,omitempty" bson:"activateOption,omitempty"`
	// ConsentList              []Consent         `json:"consentList,omitempty" bson:"consentList,omitempty"`
	// UrlData                  string            `json:"urlData,omitempty" bson:"urlData,omitempty"`
	// ExternalLinks            []ExternalLink    `json:"externalLinks,omitempty" bson:"externalLinks,omitempty"`
	// MaxTryFR                 int64             `json:"maxTryFR,omitempty" bson:"maxTryFR,omitempty"`
	// OpacityPic               int64             `json:"opacityPic,omitempty" bson:"opacityPic,omitempty"`
	// PicCardMaxSize           int64             `json:"picCardMaxSize,omitempty" bson:"picCardMaxSize,omitempty"`
	// MaxWidthOrHeight         string            `json:"maxWidthOrHeight,omitempty" bson:"maxWidthOrHeight,omitempty"`
	// PicFRMaxSize             int64             `json:"picFRMaxSize,omitempty" bson:"picFRMaxSize,omitempty"`
	// WatermarkFR              string            `json:"watermarkFR,omitempty" bson:"watermarkFR,omitempty"`
	// WatermarkNoFR            string            `json:"watermarkNoFR,omitempty" bson:"watermarkNoFR,omitempty"`
	// WatermarkOtherFR         string            `json:"watermarkOtherFR,omitempty" bson:"watermarkOtherFR,omitempty"`
	// InventoryActive          *bool             `json:"inventoryActive,omitempty" bson:"inventoryActive,omitempty"`
	// MappingServices          []MappingService  `json:"mappingService,omitempty" bson:"mappingService,omitempty"`
	// Color                    string            `json:"color,omitempty" bson:"color,omitempty"`
	// ExtraAdvance             []ExtraAdvance    `json:"extraAdvance,omitempty" bson:"extraAdvance,omitempty"`
	// CampaignConfigs          []CampaignConfigs `json:"campaignConfigs,omitempty" bson:"campaignConfigs,omitempty"`
	// AgeLimit                 string            `json:"ageLimit,omitempty" bson:"ageLimit,omitempty"`
	// AllowChannel             []string          `json:"allowChannel,omitempty" bson:"allowChannel,omitempty"`
	// ValueList                []string          `json:"valueList,omitempty" bson:"valueList,omitempty"`
	// WatermarkFRNoMsisdn      string            `json:"watermarkFRNoMsisdn,omitempty" bson:"watermarkFRNoMsisdn,omitempty"`
	// WatermarkNoFRNoMsisdn    string            `json:"watermarkNoFRNoMsisdn,omitempty" bson:"watermarkNoFRNoMsisdn,omitempty"`
	// WatermarkOtherFRNoMsisdn string            `json:"watermarkOtherFRNoMsisdn,omitempty" bson:"watermarkOtherFRNoMsisdn,omitempty"`
}
type Purpose struct {
	Collection string `json:"collection" bson:"collection"`
	Name       string `json:"name" bson:"name"`
	Value      string `json:"value" bson:"value"`
}
